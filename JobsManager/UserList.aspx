﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="JobsManager.CongViecList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="lblHello" runat="server" ForeColor="Red"></asp:Label>
        <br />
    
        <asp:GridView ID="gridJobsList" runat="server">
        </asp:GridView>
    
        <br />
        <asp:Button ID="btnLogout" runat="server" OnClick="btnLogout_Click" Text="Logout" />
    
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" Text="Job name"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtJobName" runat="server" Width="700px"></asp:TextBox>
&nbsp;<br />
        <asp:Label ID="Label2" runat="server" Text="Job descrip"></asp:Label>
&nbsp;<asp:TextBox ID="txtJobDescrip" runat="server" Width="700px"></asp:TextBox>
&nbsp;<br />
        <asp:Label ID="Label3" runat="server" Text="Start date"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtStartDate" runat="server" Width="700px"></asp:TextBox>
&nbsp;<br />
        <asp:Label ID="Label4" runat="server" Text="End date"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtEndDate" runat="server" Width="700px"></asp:TextBox>
&nbsp;<br />
        <asp:Button ID="btnAddJob" runat="server" OnClick="btnAddJob_Click" Text="Add job" />
&nbsp;<br />
    
        <asp:Label ID="lblNotify" runat="server" ForeColor="Red"></asp:Label>
    
    </div>
    </form>
</body>
</html>
