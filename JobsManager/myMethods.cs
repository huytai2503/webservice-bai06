﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace JobsManager
{
    public static class MyMethods
    {
        public static string getPassword(this string s)
        {
            byte[] bytesIn = new byte[s.Length * sizeof(char)];
            System.Buffer.BlockCopy(s.ToCharArray(), 0, bytesIn, 0, bytesIn.Length);
            SHA512Managed sha = new SHA512Managed();
            byte[] bytesOut = sha.ComputeHash(bytesIn);
            string pass = "";
            foreach (byte b in bytesOut)
                pass += b;
            return pass;
        }
    }
}