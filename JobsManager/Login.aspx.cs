﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobsManager
{
    public partial class DangNhap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LOGIN"] != null)
            {
                Response.Redirect("UserList.aspx");
            }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            dbJobsManagerDataContext database = new dbJobsManagerDataContext();
            TableUser ac = database.TableUsers.FirstOrDefault(x => 
                x.UserName == txtUsername.Text && x.Password == txtPassword.Text.getPassword());
            if(ac == null)
            {
                lblNotify.Text = "Wrong user name or password";
            }
            else
            {
                Session.Add("LOGIN", ac);
                Response.Redirect("UserList.aspx");
            }
        }

        protected void btnSignup_Click(object sender, EventArgs e)
        {
            try
            {
                dbJobsManagerDataContext database = new dbJobsManagerDataContext();
                TableUser ac = database.TableUsers.FirstOrDefault(x =>
                    x.UserName == txtUsername.Text);
                if (ac == null)
                {
                    TableUser newAC = new TableUser();
                    newAC.UserName = txtUsername.Text;
                    newAC.Password = txtPassword.Text.getPassword();
                    database.TableUsers.InsertOnSubmit(newAC);
                    database.SubmitChanges();
                    lblNotify.Text = "Account " + txtUsername.Text + " is created";
                }
                else
                {
                    lblNotify.Text = "Account " + txtUsername.Text + " is existed";
                }
            }
            catch (Exception ex)
            {
                lblNotify.Text = ex.Message;
            }
        }
    }
}