﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace JobsManager
{
    /// <summary>
    /// Summary description for MyService
    /// </summary>
    [WebService(Namespace = "http://khtn.edu/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MyService : System.Web.Services.WebService
    {
        dbJobsManagerDataContext db = new dbJobsManagerDataContext();

        [WebMethod]
        public bool Login(string userName, string password)
        {
            try
            {
                TableUser ac = db.TableUsers.FirstOrDefault(x =>
                    x.UserName == userName && x.Password == password.getPassword());
                if (ac != null)
                {
                    return true;
                }
                else return false;
            }
            catch
            {
                return false;
            }
        }

        [WebMethod]
        public string Signup(string userName, string password)
        {
            try
            {
                TableUser ac = db.TableUsers.FirstOrDefault(x => x.UserName == userName);
                if (ac == null)
                {
                    TableUser newUser = new TableUser();
                    newUser.UserName = userName;
                    newUser.Password = password.getPassword();
                    db.TableUsers.InsertOnSubmit(newUser);
                    db.SubmitChanges();
                    return "User [" + userName + "] added success";
                }
                else return "User [" + userName + "] is existed";
            }
            catch
            {
                return "Add user failed";
            }
        }

        [WebMethod]
        public int GetUserCount()
        {
            try
            {
                return db.TableUsers.Count();
            }
            catch
            {
                return -1;
            }
        }

        [WebMethod]
        public List<TableUser> GetUserList()
        {
            List<TableUser> userList = new List<TableUser>();
            try
            {
                dbJobsManagerDataContext db = new dbJobsManagerDataContext();
                userList = new List<TableUser>();
                foreach (TableUser getUser in db.TableUsers)
                {
                    TableUser mobileUser = new TableUser();
                    mobileUser.UserID = getUser.UserID;
                    mobileUser.UserName = getUser.UserName;
                    mobileUser.Password = getUser.Password;
                    userList.Add(mobileUser);
                }
                return userList;
            }
            catch
            {
                return userList;
            }
        }

        [WebMethod]
        public int GetJobCount(string userName)
        {
            try
            {
                TableUser user = db.TableUsers.FirstOrDefault(x => x.UserName == userName);
                return user.TableJobs.Count();
            }
            catch
            {
                return -1;
            }
        }

        [WebMethod]
        public List<TableJob> GetJobList(string userName)
        {
            List<TableJob> jobList = new List<TableJob>();
            try
            {
                TableUser user = db.TableUsers.FirstOrDefault(x => x.UserName == userName);
                if (user != null)
                {
                    jobList = new List<TableJob>();
                    foreach (TableJob job in user.TableJobs)
                    {
                        TableJob mobileJob = new TableJob();
                        mobileJob.JobID = job.JobID;
                        mobileJob.JobName = job.JobName;
                        mobileJob.JobDescrip = job.JobDescrip;
                        mobileJob.JobStartDate = job.JobStartDate;
                        mobileJob.JobEndDate = job.JobEndDate;
                        jobList.Add(mobileJob);
                    }
                }
                return jobList;
            }
            catch
            {
                return jobList;
            }
        }

        [WebMethod]
        public string insertNewJob(string userName, string jobName, 
            string descrip, string startDate, string endDate)
        {
            try
            {
                TableUser user = db.TableUsers.FirstOrDefault(x=>x.UserName == userName);
                TableJob newJob = new TableJob();
                newJob.JobName = jobName;
                newJob.JobDescrip = descrip;
                newJob.JobStartDate = DateTime.Parse(startDate);
                newJob.JobEndDate = DateTime.Parse(endDate);
                newJob.UserID = user.UserID;
                db.TableJobs.InsertOnSubmit(newJob);
                db.SubmitChanges();
                return "Add job success";
            }
            catch
            {
                return "Add job failed";
            }
        }
    }
}
