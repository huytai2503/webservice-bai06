﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JobsManager
{
    public partial class CongViecList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["LOGIN"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                TableUser login = (TableUser) Session["LOGIN"];
                dbJobsManagerDataContext database = new dbJobsManagerDataContext();
                gridJobsList.DataSource = login.TableJobs.ToList();
                gridJobsList.DataBind();
                lblHello.Text = "Hello " + login.UserName;
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["LOGIN"] = null;
            Session.Remove("LOGIN");
            Response.Redirect("Login.aspx");
        }

        protected void btnAddJob_Click(object sender, EventArgs e)
        {
            if(txtJobName.Text == null)
            {
                lblNotify.Text = "You must to input job name";
            }
            else
            {
                dbJobsManagerDataContext database = new dbJobsManagerDataContext();
                TableUser login = (TableUser)Session["LOGIN"];
                TableJob newJob = new TableJob();
                newJob.JobName = txtJobName.Text;
                newJob.JobDescrip = txtJobDescrip.Text;
                newJob.JobStartDate = DateTime.Parse(txtStartDate.Text);
                newJob.JobEndDate = DateTime.Parse(txtEndDate.Text);
                newJob.UserID = login.UserID;
                database.TableJobs.InsertOnSubmit(newJob);
                database.SubmitChanges();
                
                txtJobName.Text = null;
                txtJobDescrip.Text = null;
                txtStartDate.Text = null;
                txtEndDate.Text = null;
                lblNotify.Text = "The job added successful";

                gridJobsList.DataSource = login.TableJobs.ToList();
                gridJobsList.DataBind();
            }
        }
    }
}