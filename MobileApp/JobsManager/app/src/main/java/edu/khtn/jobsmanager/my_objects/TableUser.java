package edu.khtn.jobsmanager.my_objects;

/**
 * Created by Tai on 24/03/2016.
 */
public class TableUser {
    int userID;
    String userName;
    String password;

    public TableUser() {
    }

    public TableUser(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
