package edu.khtn.jobsmanager.my_objects;

import java.util.Date;

/**
 * Created by Tai on 24/03/2016.
 */
public class TableJob {
    String jobID;
    String jobName;
    String jobDescrip;
    String jobStartDate;
    String jobEndDate;
    String userID;

    public TableJob() {
    }

    public TableJob(String jobID, String jobName, String jobDescrip,
                    String jobStartDate, String jobEndDate, String userID) {
        this.jobID = jobID;
        this.jobName = jobName;
        this.jobDescrip = jobDescrip;
        this.jobStartDate = jobStartDate;
        this.jobEndDate = jobEndDate;
        this.userID = userID;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobDescrip() {
        return jobDescrip;
    }

    public void setJobDescrip(String jobDescrip) {
        this.jobDescrip = jobDescrip;
    }

    public String getJobStartDate() {
        return jobStartDate;
    }

    public void setJobStartDate(String jobStartDate) {
        this.jobStartDate = jobStartDate;
    }

    public String getJobEndDate() {
        return jobEndDate;
    }

    public void setJobEndDate(String jobEndDate) {
        this.jobEndDate = jobEndDate;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
