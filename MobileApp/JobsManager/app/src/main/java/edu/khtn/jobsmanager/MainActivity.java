package edu.khtn.jobsmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import edu.khtn.jobsmanager.my_objects.TableJob;
import edu.khtn.jobsmanager.my_objects.TableUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    EditText editUser, editPass;
    Button btnLogin, btnSignup, btnExit;
    ProgressDialog progressDialog;
    Intent intentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intentUser = new Intent(this, UserActivity.class);
        setDialog("Notification", "Please wait while connect");
        addControls();
    }

    private void addControls() {
        editUser = (EditText) findViewById(R.id.editUser);
        editPass = (EditText) findViewById(R.id.editPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        btnSignup = (Button) findViewById(R.id.btnSignup);
        btnSignup.setOnClickListener(this);
        btnExit = (Button) findViewById(R.id.btnExit);
        btnExit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String username = editUser.getText().toString();
        String password = editPass.getText().toString();
        switch (v.getId()){
            case R.id.btnLogin:
                new LoginTask().execute(username, password);
                break;
            case R.id.btnSignup:
                new SignupTask().execute(username, password);
                break;
            case R.id.btnExit:
                finish();
                break;
        }
    }

    private class LoginTask extends AsyncTask<String, Void, Boolean>{

        @Override
        protected Boolean doInBackground(String... params) {
            String userName = params[0];
            String password = params[1];
            try {
                SoapObject request = new SoapObject("http://khtn.edu/","Login");
                request.addProperty("userName", userName);
                request.addProperty("password", password);
                SoapSerializationEnvelope envelope =
                        new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                HttpTransportSE transportSE = new HttpTransportSE
                        ("http://192.168.43.108/JobsManager/MyService.asmx");
                transportSE.call("http://khtn.edu/Login", envelope);
                SoapPrimitive ret = (SoapPrimitive) envelope.getResponse();
                boolean result = Boolean.parseBoolean(ret.toString());
                return  result;
            }catch (Exception e){
                System.out.println("my error: "+e.toString());
                return false;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            progressDialog.dismiss();
            if(aBoolean) {
                intentUser.putExtra("USERNAME", editUser.getText().toString());
                startActivity(intentUser);
            }else
                showToast("Login failed");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    private class SignupTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            String userName = params[0];
            String password = params[1];
            if (userName != null && password != null){
                try {
                    SoapObject request = new SoapObject("http://khtn.edu/","Signup");
                    request.addProperty("userName", userName);
                    request.addProperty("password", password);
                    SoapSerializationEnvelope envelope =
                            new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);
                    HttpTransportSE transportSE = new HttpTransportSE
                            ("http://192.168.43.108/JobsManager/MyService.asmx");
                    transportSE.call("http://khtn.edu/Signup", envelope);
                    SoapPrimitive ret = (SoapPrimitive) envelope.getResponse();
                    String result = ret.toString();
                    return  result;
                }catch (Exception e){
                    System.out.println("my error: "+e.toString());
                    return "Can not connect to server";
                }
            }else {
                return "Input userName and Password";
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            showToast(result);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    protected void showToast(String msg){
        Toast.makeText(MainActivity.this,msg,Toast.LENGTH_LONG).show();
    }

    protected void setDialog(String tiltle, String msg){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(tiltle);
        progressDialog.setMessage(msg
        );
    }
}
