package edu.khtn.jobsmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.khtn.jobsmanager.my_adpters.AdapterGrid;

public class UserActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnUserCount, btnJobCount, btnJobList, btnUserList, btnLogout, btnAddJob;
    TextView txtUserName, txtUserCount, txtJobCount;
    AdapterGrid adapterUser, adapterJob;
    Intent intentAdd;
    ProgressDialog dialog;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        addControls();
        setDialog("Notify", "Please wait while connect");
        Intent intent = getIntent();
        String userName = intent.getStringExtra("USERNAME");
        txtUserName.setText(userName);
        intentAdd = new Intent(this,AddActivity.class);
        intentAdd.putExtra("USERNAME", txtUserName.getText().toString());
    }

    @Override
    public void onClick(View v) {
        String userName = txtUserName.getText().toString();
        switch (v.getId()){
            case R.id.btnUserCount:
                new UserCountTask().execute();
                break;
            case R.id.btnJobCount:
                new JobCountTask().execute(userName);
                break;
            case R.id.btnUserList:
                new UserListTask().execute();
                break;
            case R.id.btnJobList:
                new JobListTask().execute(userName);
                break;
            case R.id.btnAddJob:
                startActivity(intentAdd);
                break;
            case R.id.btnLogout:
                finish();
                break;
        }
    }

    private class UserCountTask extends AsyncTask<Integer,Void,Integer> {

        @Override
        protected Integer doInBackground(Integer... params) {
            try {
                SoapObject request = new SoapObject("http://khtn.edu/","GetUserCount");
                SoapSerializationEnvelope envelope =
                        new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                HttpTransportSE transportSE = new HttpTransportSE
                        ("http://192.168.43.108/JobsManager/MyService.asmx");
                transportSE.call("http://khtn.edu/GetUserCount",envelope);
                SoapPrimitive ret = (SoapPrimitive) envelope.getResponse();
                int count = Integer.parseInt(ret.toString());
                return count;
            }catch (Exception e){
                return -1;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            txtUserCount.setText("= " + integer);
            dialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    private class UserListTask extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params) {
            try{
                SoapObject request = new SoapObject("http://khtn.edu/", "GetUserList");
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                HttpTransportSE transportSE = new HttpTransportSE
                        ("http://192.168.43.108/JobsManager/MyService.asmx");
                transportSE.call("http://khtn.edu/GetUserList", envelope);
                SoapObject ret = (SoapObject) envelope.getResponse();
                return ret.toString();
            }catch (Exception e){
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (result != null){
                List<String> userList = addUserToList(result);
                adapterUser = new AdapterGrid(getBaseContext());
                adapterUser.setObjectList(userList);
                gridView.setAdapter(adapterUser);
            }else {
                showToast("Can not get result");
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    private class JobCountTask extends AsyncTask<String,Void,Integer>{

        @Override
        protected Integer doInBackground(String... params) {
            String userName = params[0];
            try {
                SoapObject request = new SoapObject("http://khtn.edu/","GetJobCount");
                request.addProperty("userName", userName);
                SoapSerializationEnvelope envelope =
                        new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                HttpTransportSE transportSE = new HttpTransportSE
                        ("http://192.168.43.108/JobsManager/MyService.asmx");
                transportSE.call("http://khtn.edu/GetJobCount", envelope);
                SoapPrimitive ret = (SoapPrimitive) envelope.getResponse();
                System.out.println("envelope.getResponse(): " + ret.toString());
                int count = Integer.parseInt(ret.toString());
                return count;
            }catch (Exception e){
                System.out.println("my error: "+e.toString());
                return -1;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            txtJobCount.setText("= " + integer);
            dialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    private class JobListTask extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params) {
            String userName = params[0];
            try{
                SoapObject request = new SoapObject("http://khtn.edu/","GetJobList");
                request.addProperty("userName", userName);
                SoapSerializationEnvelope envelope =
                        new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                HttpTransportSE transportSE = new HttpTransportSE
                        ("http://192.168.43.108/JobsManager/MyService.asmx");
                transportSE.call("http://khtn.edu/GetJobList", envelope);
                SoapObject ret = (SoapObject) envelope.getResponse();
                return ret.toString();
            }catch (Exception e){
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (result != null){
                List<String> jobList = addJobToList(result);
                adapterJob = new AdapterGrid(getBaseContext());
                adapterJob.setObjectList(jobList);
                gridView.setAdapter(adapterJob);
            }else {
                showToast("Can not get result");
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    protected List<String> addJobToList(String source){
        List<String> list = new ArrayList<>();
        try{
            source = source.replace("anyType","");
            source = source.replace("TableJob=","");
            source = source.replace("{","");
            source = source.replace("}","");
            String[] jobArr = source.split("; ; ");
            for (String s : jobArr){
                String[] detailArr = s.split("; ");
                String jobID = detailArr[0].split("=")[1];
                String jobName = detailArr[1].split("=")[1];
                String jobDescrip = detailArr[2].split("=")[1];
                String jobStartDate = detailArr[3].split("=")[1].split("T")[0];
                String jobEndDate = detailArr[4].split("=")[1].split("T")[0];
                String userID = detailArr[5].split("=")[1];
                list.add("jobID: " + jobID);
                list.add("JobName: \n" + jobName);
                list.add("Descrip: \n" + jobDescrip);
                list.add("StartDate: " + jobStartDate);
                list.add("EndDate: " + jobEndDate);
                list.add(" ");
            }
        }catch (Exception e){
            return list;
        }
        return list;
    }

    protected List<String> addUserToList(String source){
        List<String> list = new ArrayList<>();
        try{
            source = source.replace("anyType","");
            source = source.replace("TableJob=","");
            source = source.replace("TableJobs=","");
            source = source.replace("TableUser=","");
            source = source.replace("{","");
            source = source.replace("}","");
            String[] userArr = source.split("; ; ; ");
            for (String s : userArr){
                String[] detailArr = s.split("; ");
                String userID = detailArr[0].split("=")[1];
                String userName = detailArr[1].split("=")[1];
                String password = detailArr[2].split("=")[1];
                list.add("UserID: " + userID);
                list.add("UserName: " + userName);
                list.add("Password: \n" + password);
                list.add(" ");
            }
        }catch (Exception e){
            return list;
        }
        return list;
    }

    protected void showToast(String msg){
        Toast.makeText(UserActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    protected void setDialog(String tiltle, String msg){
        dialog = new ProgressDialog(this);
        dialog.setTitle(tiltle);
        dialog.setMessage(msg
        );
    }

    private void addControls() {
        btnUserCount = (Button) findViewById(R.id.btnUserCount);
        btnUserCount.setOnClickListener(this);
        btnJobCount = (Button) findViewById(R.id.btnJobCount);
        btnJobCount.setOnClickListener(this);
        btnUserList = (Button) findViewById(R.id.btnUserList);
        btnUserList.setOnClickListener(this);
        btnJobList = (Button) findViewById(R.id.btnJobList);
        btnJobList.setOnClickListener(this);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);
        btnAddJob = (Button) findViewById(R.id.btnAddJob);
        btnAddJob.setOnClickListener(this);
        gridView = (GridView) findViewById(R.id.gridView);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtJobCount = (TextView) findViewById(R.id.txtJobCount);
        txtUserCount = (TextView) findViewById(R.id.txtUserCount);
    }
}
