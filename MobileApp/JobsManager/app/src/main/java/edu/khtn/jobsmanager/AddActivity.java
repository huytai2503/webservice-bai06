package edu.khtn.jobsmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Tai on 25/03/2016.
 */
public class AddActivity extends AppCompatActivity{
    EditText editJobName, editDescrip, editStartDate, editEndDate;
    TextView txtUserName;
    String[] arr;
    ProgressDialog dialog;
    Button btnPost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        addControls();
        setDialog("Notify", "Please wait while connect");
        Intent intent = getIntent();
        String userName = intent.getStringExtra("USERNAME");
        txtUserName.setText(userName);
        arr = new String[5];
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editJobName.getText().toString()==null){
                    showToast("Input job name");
                }
                else{
                    arr[0] = txtUserName.getText().toString();
                    arr[1] = editJobName.getText().toString();
                    arr[2] = editDescrip.getText().toString();
                    arr[3] = editStartDate.getText().toString();
                    arr[4] = editEndDate.getText().toString();
                    new AddJobTask().execute(arr);
                }
            }
        });
    }

    private class AddJobTask extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                SoapObject request = new SoapObject("http://khtn.edu/","insertNewJob");
                request.addProperty("userName",params[0]);
                request.addProperty("jobName",params[1]);
                request.addProperty("descrip",params[2]);
                request.addProperty("startDate",params[3]);
                request.addProperty("endDate", params[4]);
                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet=true;
                envelope.setOutputSoapObject(request);
                HttpTransportSE transportSE = new HttpTransportSE
                        ("http://192.168.43.108/JobsManager/MyService.asmx");
                transportSE.call("http://khtn.edu/insertNewJob",envelope);
                SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
                return result.toString();
            } catch (Exception e) {
                System.out.println("my error: " + e.toString());
                return "Can not add new job to server";
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            showToast(result);
            editJobName.setText("");
            editDescrip.setText("");
            editStartDate.setText("");
            editEndDate.setText("");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    protected void showToast(String msg){
        Toast.makeText(AddActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    protected void setDialog(String tiltle, String msg){
        dialog = new ProgressDialog(this);
        dialog.setTitle(tiltle);
        dialog.setMessage(msg
        );
    }

    private void addControls() {
        btnPost = (Button) findViewById(R.id.btnPost);
        editJobName = (EditText) findViewById(R.id.editJobName);
        editDescrip = (EditText) findViewById(R.id.editDescrip);
        editStartDate = (EditText) findViewById(R.id.editStartDate);
        editEndDate = (EditText) findViewById(R.id.editEndDate);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
    }

}
