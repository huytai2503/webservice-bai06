package edu.khtn.jobsmanager.my_adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.khtn.jobsmanager.R;

/**
 * Created by Tai on 24/03/2016.
 */
public class AdapterGrid extends BaseAdapter {
    Context context;
    List objectList = new ArrayList();

    public AdapterGrid(Context context) {
        this.context = context;
    }

    public AdapterGrid(Context context, List objectList) {
        this.context = context;
        this.objectList = objectList;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public List getObjectList() {
        return objectList;
    }

    public void setObjectList(List objectList) {
        this.objectList = objectList;
    }

    @Override
    public int getCount() {
        return objectList.size();
    }

    @Override
    public Object getItem(int position) {
        return objectList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.model_grid, null, false);
        TextView textView = (TextView) view.findViewById(R.id.txtModel);
        textView.setText(objectList.get(position).toString());
        if (position%2==0){
            textView.setBackgroundResource(R.color.lightGray);
        }
        return view;
    }
}
